import React, {Component} from 'react';
import classes from './App.module.css';
import Person from './Person/Person';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';
//import Radium, { StyleRoot } from 'radium';
import styled from 'styled-components';

const StyledButton = styled.button`
	background-color: ${props => props.alt ? 'red' : 'green'};
	color: white;
	font: inherit;
	border: 1px solid blue;
	padding: 8px;
	cursos: pointer;
	
	&:hover {
		background-color: ${props => props.alt ? 'salmon' : 'lightgreen'};
		color: black;
	}
`;

class App extends Component {
	state = {
		persons: [
			{id: "asdcwe", name: "Max", age:30},
			{id: "gvgtd", name: "Ju", age:28},
			{id: "trhzdga", name: "Lucas", age:47}
		],
		otherState: 'algum outro valor',
		outPutUserName: 'Vamos ver esse nome no Output!!!',
		showPersons: false
	}

	switchNameHandler = () => {
		//console.log("O botão foi clicado");
		//DONT'T DO THIS: this.state.persons[0].name = "Raul";
		this.setState({
			persons: [
				{name: "Jorge", age:30},
				{name: "Kleber", age:28},
				{name: "Diguinho", age:47}
			]
		});
	}

	nameChangedHandlerOld = (event) => {
		this.setState({
			persons: [
				{name: "Jorge", age:30},
				{name: event.target.value, age:28},
				{name: "Diguinho", age:47}
			]
		});
	}

	switchOutputUsernameHandler = (event) => {
		//console.log("O botão foi clicado");
		//DONT'T DO THIS: this.state.persons[0].name = "Raul";
		this.setState({
			outPutUserName: event.target.value
		});
	}

	togglePersonsHandler = () => {
		const doesShow = this.state.showPersons;
		this.setState({showPersons: !doesShow});
	}

	deletePersonsHandler = (personIndex) => {
		//const persons = this.state.persons.slice();
		const persons = [...this.state.persons];
		persons.splice(personIndex, 1);
		this.setState({persons: persons})
	}

	nameChangedHandler = (event, id) => {
		const personIndex = this.state.persons.findIndex(p => {
			return p.id  === id;
		});

		//const person = Object.assign({}, this.state.persons[personIndex]);

		const person = {
			...this.state.persons[personIndex]
		};

		person.name = event.target.value;

		const persons = [...this.state.persons];
		persons[personIndex] = person;

		this.setState({ persons: persons });
	}

	render() {
		const style = {
			backgroundColor: 'green',
			color: 'white',
			font: 'inherit',
			border: '1px solid blue',
			padding: '8px',
			cursos: 'pointer',
			':hover': {
				backgroundColor: 'lightgreen',
				color: 'black'
			}
		};

		let buttonClass = '';

		let persons = null;

		if ( this.state.showPersons ) {
			persons = (
				<div>
					{this.state.persons.map((person, index) => {
						return <Person
							click={() => this.deletePersonsHandler(index)}
							name={person.name}
							age={person.age}
							key={person.id} 
							changed={(event) => this.nameChangedHandler(event, person.id)} />

					})}
				</div> 

				/*
				<Person 
					name={this.state.persons[0].name} 
					idade="19"/>
				<Person 
					name={this.state.persons[1].name} 
					idade="28"
					click={this.switchNameHandler.bind(this, 'Douglas!!!')}
					changed={this.nameChangedHandler}>My hobbies: Cavaco
				</Person>

				<Person 
					name={this.state.persons[2].name} 
					idade="34"/>
				*/
			);	

			// style.backgroundColor = 'red';
			// style[':hover'] = {
			// 	backgroundColor: 'salmon',
			// 	color: 'black'
			// }

			buttonClass = classes.Red;
		}

		return (
				<div className={classes.App}>
					<h1>Stackoverflow</h1>
					<p>está realmente funcionando</p>
					{/*<button onClick={this.switchNameHandler}>Mude o nome</button>*/}
					
					<button className={buttonClass} onClick={this.togglePersonsHandler}>Toggle persons 2</button>
					<UserInput name={this.state.outPutUserName} changed={this.switchOutputUsernameHandler}/>
					<UserOutput username='Nome de usuário do output!!!'/>
					<UserOutput username={this.state.outPutUserName}/>
					{persons}
				</div>
		);

		//return React.createElement('div', null, 'h1', 'Olá, Eu sou React App');

		//return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Funcionou agora?'));
	}
}

//export default Radium(App);
export default App;